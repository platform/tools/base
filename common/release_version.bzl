#buildifier: disable=module-docstring
BASE_VERSION = "31.10.0-alpha08"
BUILD_VERSION = "8.10.0-alpha08"
COMMANDLINE_TOOLS_VERSION = "20.0-alpha01"

# These are used for nightly releases
LAST_STABLE_BUILD_VERSION = "8.9.0"
