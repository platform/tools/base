/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.testing.utils

import org.junit.Test
import kotlin.test.assertEquals

class SystemImageHashUtilsTest {

    @Test
    fun testComputeSystemImageHashFromDsl() {
        var expectedHash = "system-images;android-29;default;x86"
        var computedHash = computeSystemImageHashFromDsl(29, null, "aosp", "", "x86")

        assertEquals(expectedHash, computedHash)

        expectedHash = "system-images;android-30;google_apis;x86_64"
        computedHash = computeSystemImageHashFromDsl(30, null, "google", "", "x86_64")

        assertEquals(expectedHash, computedHash)

        expectedHash = "system-images;android-31;android-wear;x86_64"
        computedHash = computeSystemImageHashFromDsl(31, null,"android-wear", "", "x86_64")

        assertEquals(expectedHash, computedHash)

        expectedHash = "system-images;android-31;google_apis_ps16k;arm64-v8a"
        computedHash = computeSystemImageHashFromDsl(31, null, "google", "_ps16k", "arm64-v8a")

        assertEquals(expectedHash, computedHash)
    }

    @Test
    fun testGetPageAlignmentSuffix() {
        assertEquals(
            null,
            getPageAlignmentSuffix("google_apis")
        )

        assertEquals(
            null,
            getPageAlignmentSuffix("default")
        )

        assertEquals(
            "_ps16k",
            getPageAlignmentSuffix("google_apis_ps16k")
        )

        // if we end up supporting other page sizes in the future
        assertEquals(
            "_ps32k",
            getPageAlignmentSuffix("default_ps32k")
        )
    }

    @Test
    fun testParseApiFromHash() {
        assertEquals(29, parseApiFromHash("system-images;android-29;default;x86"))
        assertEquals(24, parseApiFromHash("system-images;android-24;default;arm64-v8a"))
        assertEquals(
            30, parseApiFromHash("system-images;android-30;google_apis_playstore;x86_64")
        )
        assertEquals(
            30,
            parseApiFromHash("system-images;android-30-ext12;default;x86_64"))
    }

    @Test
    fun testParseExtensionFromHash() {
        assertEquals(
            null,
            parseExtensionFromHash("system-images;android-29;default;x86"))
        assertEquals(
            9,
            parseExtensionFromHash("system-images;android-29-ext9;google_apis;x86"))
        assertEquals(
            12,
            parseExtensionFromHash("system-images;android-30-ext12;default;x86_64"))
    }

    @Test
    fun testParseSystemImageSource() {
        assertEquals(
            "google_apis_playstore",
            parseSystemImageSourceFromHash(
                "system-images;android-30;google_apis_playstore;x86_64"))
        assertEquals(
            "google_apis_playstore",
            parseSystemImageSourceFromHash(
                "system-images;android-36;google_apis_playstore_ps16k;x86_64"))
    }

    @Test
    fun testParseVendor() {
        assertEquals(
            "google_apis_playstore",
            parseVendorFromHash(
                "system-images;android-30;google_apis_playstore;x86_64"))
        // Vendor string will include page size suffix.
        assertEquals(
            "google_apis_playstore_ps16k",
            parseVendorFromHash(
                "system-images;android-36;google_apis_playstore_ps16k;x86_64"))
    }
}
