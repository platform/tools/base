/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.build.gradle.integration.common.fixture.project.options

import com.android.build.gradle.integration.common.fixture.project.builder.BuildFileType


interface CreationOptionsBuilder {
    var rootFolderName: String
    var buildFileType: BuildFileType
}

data class CreationOptions(
    val name: String,
    val buildFileType: BuildFileType
) {
    companion object {
        const val DEFAULT_BUILD_NAME: String = "project"
    }
}

class CreationOptionsDelegate: CreationOptionsBuilder, MergeableOptions<CreationOptionsDelegate> {

    override var rootFolderName: String = CreationOptions.DEFAULT_BUILD_NAME
    override var buildFileType: BuildFileType = BuildFileType.GROOVY

    val asCreationOptions: CreationOptions
        get() = CreationOptions(rootFolderName, buildFileType)

    override fun mergeWith(other: CreationOptionsDelegate) {
        this.rootFolderName = other.rootFolderName
        this.buildFileType = other.buildFileType
    }
}
