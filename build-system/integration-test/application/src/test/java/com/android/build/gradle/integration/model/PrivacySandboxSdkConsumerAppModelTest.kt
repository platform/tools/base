/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.build.gradle.integration.model

import com.android.build.gradle.integration.common.fixture.model.ModelComparator
import com.android.build.gradle.integration.common.fixture.project.GradleRule
import com.android.build.gradle.integration.common.fixture.project.prebuilts.HelloWorldAndroid
import com.android.build.gradle.integration.common.fixture.testprojects.prebuilts.privacysandbox.createGradleProjectWithPrivacySandboxLibrary
import com.android.builder.model.v2.ide.SyncIssue
import org.junit.Rule
import org.junit.Test

class PrivacySandboxSdkConsumerAppModelTest : ModelComparator() {
    @get:Rule
    val rule = GradleRule.from {
        createGradleProjectWithPrivacySandboxLibrary {}
        androidApplication(":app") {
            android {
                defaultConfig.minSdk = 33
            }
            HelloWorldAndroid.setupJava(files)
            dependencies {
                implementation(project(":privacy-sandbox-sdk"))
            }
        }
    }


    @Test
    fun `test models`() {
        val result = rule.build
            .modelBuilder
            .ignoreSyncIssues(SyncIssue.SEVERITY_WARNING)
            .fetchModels(variantName = "debug")

        with(result).compareBasicAndroidProject(goldenFile = "BasicAndroidProject", projectAction = { getProject(":app") })
        with(result).compareAndroidProject(goldenFile = "AndroidProject", projectAction = { getProject(":app") })
        with(result).compareAndroidDsl(goldenFile = "AndroidDsl", projectAction = { getProject(":app") })
        with(result).compareVariantDependencies(goldenFile = "VariantDependencies", projectAction = { getProject(":app") })
    }
}
