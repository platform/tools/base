> VariantDependencies:
   - name                 = "debug"
   > mainArtifact:
      - compileDependencies:
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-api|com.google.guava:guava:19.0:
                  - requestedCoordinates = (null)
                  - dependencies         = []
      - runtimeDependencies:
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-runtime|com.google.guava:guava:19.0:
                  - requestedCoordinates = (null)
                  - dependencies         = []
      - unresolvedDependencies = []
   < mainArtifact
   > androidTestArtifact:
      > compileDependencies:
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-api|com.google.guava:guava:19.0:
                  - requestedCoordinates = (null)
                  - dependencies         = []
         - :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-api|project:lib:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
                  - dependencies = "*visited*"
      < compileDependencies
      > runtimeDependencies:
         - :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-runtime|project:lib:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
                  - requestedCoordinates = (null)
                  - dependencies:
                     - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-runtime|com.google.guava:guava:19.0:
                        - requestedCoordinates = (null)
                        - dependencies         = []
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
            - dependencies = "*visited*"
      < runtimeDependencies
      - unresolvedDependencies = []
   < androidTestArtifact
   > unitTestArtifact:
      > compileDependencies:
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-api|com.google.guava:guava:19.0:
                  - requestedCoordinates = (null)
                  - dependencies         = []
         - :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-api|project:lib:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
                  - dependencies = "*visited*"
      < compileDependencies
      > runtimeDependencies:
         - :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-runtime|project:lib:unspecified:
            - requestedCoordinates = (null)
            - dependencies:
               - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
                  - requestedCoordinates = (null)
                  - dependencies:
                     - com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-runtime|com.google.guava:guava:19.0:
                        - requestedCoordinates = (null)
                        - dependencies         = []
         - :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
            - dependencies = "*visited*"
      < runtimeDependencies
      - unresolvedDependencies = []
   < unitTestArtifact
   - testFixturesArtifact = (null)
   > libraries:
      > :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified:
         - type               = PROJECT
         - artifact           = {PROJECT}/jar/build/libs/jar.jar{!}
         > projectInfo:
            - buildId        = ":"
            - projectPath    = ":jar"
            - isTestFixtures = false
            - buildType      = (null)
            - productFlavors = []
            - attributes:
               * "org.gradle.category -> library"
               * "org.gradle.dependency.bundling -> external"
               * "org.gradle.jvm.version -> {Java_Version}"
               * "org.gradle.libraryelements -> jar"
               * "org.gradle.usage -> java-api"
            - capabilities:
               * "project:jar:unspecified"
         < projectInfo
         - libraryInfo        = (null)
         - lintJar            = (null)
         - androidLibraryData = (null)
      < :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-api|project:jar:unspecified
      > :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified:
         - type               = PROJECT
         - artifact           = {PROJECT}/jar/build/libs/jar.jar{!}
         > projectInfo:
            - buildId        = ":"
            - projectPath    = ":jar"
            - isTestFixtures = false
            - buildType      = (null)
            - productFlavors = []
            - attributes:
               * "org.gradle.category -> library"
               * "org.gradle.dependency.bundling -> external"
               * "org.gradle.jvm.version -> {Java_Version}"
               * "org.gradle.libraryelements -> jar"
               * "org.gradle.usage -> java-runtime"
            - capabilities:
               * "project:jar:unspecified"
         < projectInfo
         - libraryInfo        = (null)
         - lintJar            = (null)
         - androidLibraryData = (null)
      < :|:jar|org.gradle.category>library, org.gradle.dependency.bundling>external, org.gradle.jvm.version>{Java_Version}, org.gradle.libraryelements>jar, org.gradle.usage>java-runtime|project:jar:unspecified
      > :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-api|project:lib:unspecified:
         - type               = PROJECT
         - artifact           = (null)
         > projectInfo:
            - buildId        = ":"
            - projectPath    = ":lib"
            - isTestFixtures = false
            - buildType      = "debug"
            - productFlavors = []
            - attributes:
               * "com.android.build.api.attributes.AgpVersionAttr -> {AGP_Version}"
               * "com.android.build.gradle.internal.attributes.VariantAttr -> debug"
               * "org.gradle.usage -> java-api"
            - capabilities:
               * "project:lib:unspecified"
         < projectInfo
         - libraryInfo        = (null)
         - lintJar            = (null)
         - androidLibraryData = (null)
      < :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-api|project:lib:unspecified
      > :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-runtime|project:lib:unspecified:
         - type               = PROJECT
         - artifact           = (null)
         > projectInfo:
            - buildId        = ":"
            - projectPath    = ":lib"
            - isTestFixtures = false
            - buildType      = "debug"
            - productFlavors = []
            - attributes:
               * "com.android.build.api.attributes.AgpVersionAttr -> {AGP_Version}"
               * "com.android.build.gradle.internal.attributes.VariantAttr -> debug"
               * "org.gradle.usage -> java-runtime"
            - capabilities:
               * "project:lib:unspecified"
         < projectInfo
         - libraryInfo        = (null)
         - lintJar            = (null)
         - androidLibraryData = (null)
      < :|:lib|debug|com.android.build.api.attributes.AgpVersionAttr>{AGP_Version}, com.android.build.gradle.internal.attributes.VariantAttr>debug, org.gradle.usage>java-runtime|project:lib:unspecified
      > com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-api|com.google.guava:guava:19.0:
         - type               = JAVA_LIBRARY
         - artifact           = {LOCAL_REPO}/com/google/guava/guava/19.0/guava-19.0.jar{F}
         - projectInfo        = (null)
         > libraryInfo:
            - group          = "com.google.guava"
            - name           = "guava"
            - version        = "19.0"
            - isTestFixtures = false
            - buildType      = (null)
            - productFlavors = []
            - attributes:
               * "org.gradle.category -> library"
               * "org.gradle.libraryelements -> jar"
               * "org.gradle.status -> release"
               * "org.gradle.usage -> java-api"
            - capabilities:
               * "com.google.guava:guava:19.0"
         < libraryInfo
         - lintJar            = (null)
         - androidLibraryData = (null)
      < com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-api|com.google.guava:guava:19.0
      > com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-runtime|com.google.guava:guava:19.0:
         - type               = JAVA_LIBRARY
         - artifact           = {LOCAL_REPO}/com/google/guava/guava/19.0/guava-19.0.jar{F}
         - projectInfo        = (null)
         > libraryInfo:
            - group          = "com.google.guava"
            - name           = "guava"
            - version        = "19.0"
            - isTestFixtures = false
            - buildType      = (null)
            - productFlavors = []
            - attributes:
               * "org.gradle.category -> library"
               * "org.gradle.libraryelements -> jar"
               * "org.gradle.status -> release"
               * "org.gradle.usage -> java-runtime"
            - capabilities:
               * "com.google.guava:guava:19.0"
         < libraryInfo
         - lintJar            = (null)
         - androidLibraryData = (null)
      < com.google.guava|guava|19.0|org.gradle.category>library, org.gradle.libraryelements>jar, org.gradle.status>release, org.gradle.usage>java-runtime|com.google.guava:guava:19.0
   < libraries
< VariantDependencies
