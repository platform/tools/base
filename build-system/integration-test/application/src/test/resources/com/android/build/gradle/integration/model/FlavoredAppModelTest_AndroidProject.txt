> AndroidProject:
   - namespace             = "pkg.name.app"
   - androidTestNamespace  = "pkg.name.app.test"
   - testFixturesNamespace = (null)
   - resourcePrefix        = (null)
   - dynamicFeatures       = []
   > variants:
      > proDebug:
         - name                   = "proDebug"
         - displayName            = "proDebug"
         - isInstantAppCompatible = false
         > mainArtifact:
            - compileTaskName               = "compileProDebugSources"
            - assembleTaskName              = "assembleProDebug"
            - classesFolders:
               * {PROJECT}/app/build/intermediates/compile_and_runtime_not_namespaced_r_class_jar/proDebug/processProDebugResources/R.jar{!}
               * {PROJECT}/app/build/intermediates/javac/proDebug/compileProDebugJavaWithJavac/classes{!}
            - ideSetupTaskNames             = ["generateProDebugSources"]
            - generatedSourceFolders:
               * {PROJECT}/app/build/generated/ap_generated_sources/proDebug/out{!}
            - minSdkVersion:
               - apiLevel = 1
               - codename = (null)
            - targetSdkVersionOverride      = (null)
            - maxSdkVersion                 = (null)
            - isSigned                      = true
            - signingConfigName             = "debug"
            - applicationId                 = "pkg.name.app"
            - sourceGenTaskName             = "generateProDebugSources"
            - resGenTaskName                = "generateProDebugResources"
            - generatedResourceFolders:
               * {PROJECT}/app/build/generated/res/resValues/pro/debug{!}
            - generatedAssetsFolders        = []
            - abiFilters                    = []
            - assembleTaskOutputListingFile = {PROJECT}/app/build/intermediates/apk_ide_redirect_file/proDebug/createProDebugApkListingFileRedirect/redirect.txt{!}
            - testInfo                      = (null)
            - bundleInfo:
               - bundleTaskName                     = "bundleProDebug"
               - bundleTaskOutputListingFile        = {PROJECT}/app/build/intermediates/bundle_ide_redirect_file/proDebug/createProDebugBundleListingFileRedirect/redirect.txt{!}
               - apkFromBundleTaskName              = "extractApksForProDebug"
               - apkFromBundleTaskOutputListingFile = {PROJECT}/app/build/intermediates/apk_from_bundle_ide_redirect_file/proDebug/createProDebugApksFromBundleListingFileRedirect/redirect.txt{!}
            - codeShrinker                  = (null)
            - privacySandboxSdkInfo         = (null)
         < mainArtifact
         > androidTestArtifact:
            - compileTaskName               = "compileProDebugAndroidTestSources"
            - assembleTaskName              = "assembleProDebugAndroidTest"
            - classesFolders:
               * {PROJECT}/app/build/intermediates/compile_and_runtime_not_namespaced_r_class_jar/proDebugAndroidTest/processProDebugAndroidTestResources/R.jar{!}
               * {PROJECT}/app/build/intermediates/javac/proDebugAndroidTest/compileProDebugAndroidTestJavaWithJavac/classes{!}
            - ideSetupTaskNames             = ["generateProDebugAndroidTestSources"]
            - generatedSourceFolders:
               * {PROJECT}/app/build/generated/ap_generated_sources/proDebugAndroidTest/out{!}
            - minSdkVersion:
               - apiLevel = 1
               - codename = (null)
            - targetSdkVersionOverride      = (null)
            - maxSdkVersion                 = (null)
            - isSigned                      = true
            - signingConfigName             = "debug"
            - applicationId                 = "pkg.name.app.test"
            - sourceGenTaskName             = "generateProDebugAndroidTestSources"
            - resGenTaskName                = "generateProDebugAndroidTestResources"
            - generatedResourceFolders:
               * {PROJECT}/app/build/generated/res/resValues/androidTest/pro/debug{!}
            - generatedAssetsFolders        = []
            - abiFilters                    = []
            - assembleTaskOutputListingFile = {PROJECT}/app/build/intermediates/apk_ide_redirect_file/proDebugAndroidTest/createProDebugAndroidTestApkListingFileRedirect/redirect.txt{!}
            - testInfo:
               - animationsDisabled       = false
               - execution                = HOST
               - additionalRuntimeApks    = []
               - instrumentedTestTaskName = "connectedProDebugAndroidTest"
            - bundleInfo                    = (null)
            - codeShrinker                  = (null)
            - privacySandboxSdkInfo         = (null)
         < androidTestArtifact
         > unitTestArtifact:
            - compileTaskName       = "compileProDebugUnitTestSources"
            - assembleTaskName      = "assembleProDebugUnitTest"
            - classesFolders:
               * {PROJECT}/app/build/intermediates/compile_and_runtime_not_namespaced_r_class_jar/proDebug/processProDebugResources/R.jar{!}
               * {PROJECT}/app/build/intermediates/javac/proDebugUnitTest/compileProDebugUnitTestJavaWithJavac/classes{!}
            - ideSetupTaskNames     = ["createMockableJar"]
            - generatedSourceFolders:
               * {PROJECT}/app/build/generated/ap_generated_sources/proDebugUnitTest/out{!}
            - mockablePlatformJar   = {GRADLE_CACHE}/{CHECKSUM}/transformed/android.jar{F}
            - runtimeResourceFolder = {PROJECT}/app/build/intermediates/java_res/proDebugUnitTest/processProDebugUnitTestJavaRes/out{!}
         < unitTestArtifact
         - testFixturesArtifact   = (null)
         - testedTargetVariant    = (null)
         - desugaredMethods       = [{GRADLE_CACHE}/{CHECKSUM}/transformed/D8BackportedDesugaredMethods.txt{F}]
      < proDebug
   < variants
   - javaCompileOptions:
      - encoding                       = "UTF-8"
      - sourceCompatibility            = "1.8"
      - targetCompatibility            = "1.8"
      - isCoreLibraryDesugaringEnabled = false
   - viewBindingOptions:
      - isEnabled = false
   > flags:
      * "APPLICATION_R_CLASS_CONSTANT_IDS -> false"
      * "BUILD_FEATURE_ANDROID_RESOURCES -> true"
      * "DATA_BINDING_ENABLED -> false"
      * "ENABLE_VCS_INFO -> false"
      * "EXCLUDE_LIBRARY_COMPONENTS_FROM_CONSTRAINTS -> false"
      * "GENERATE_MANIFEST_CLASS -> false"
      * "JETPACK_COMPOSE -> false"
      * "ML_MODEL_BINDING -> false"
      * "TEST_R_CLASS_CONSTANT_IDS -> false"
      * "TRANSITIVE_R_CLASS -> false"
      * "UNIFIED_TEST_PLATFORM -> true"
      * "USE_ANDROID_X -> false"
   < flags
   - lintChecksJars        = []
< AndroidProject
